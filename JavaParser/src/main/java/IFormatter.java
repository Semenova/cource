import java.io.IOException;

/**
 * Created by Svetlana on 19.11.2015.
 */
public interface IFormatter {
    public void format(IScanner scan, IWriter writer) throws IOException;
}
