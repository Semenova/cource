import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Svetlana on 19.11.2015.
 */
public class CustomWriter implements IWriter {

    FileWriter writer;
    CustomWriter (String filePath) throws IOException {
        writer = new FileWriter(filePath);

    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void append(String str) throws IOException {
        writer.append(str);
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    @Override
    public void append(char chr) throws IOException {
        writer.append(chr);
    }
}
