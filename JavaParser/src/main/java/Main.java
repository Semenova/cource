import java.io.IOException;

/**
 * Created by Svetlana on 18.11.2015.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        IScanner fscan = (IScanner) new CustomScanner("input.txt");
        IWriter writer = new CustomWriter("output.txt");
        IFormatter formatter = new Formatter();
        formatter.format(fscan, writer);
    }
}
