import java.io.IOException;

/**
 * Created by Svetlana on 19.11.2015.
 */
public interface IWriter {
    public void flush() throws IOException;
    public void append(String str) throws IOException;
    public void close() throws IOException;
    public void append (char chr) throws IOException;
}
