import java.io.IOException;

/**
 * Created by Svetlana on 19.11.2015.
 */
public class Formatter implements IFormatter {
    private static String tab(int openBrackets, String output) {
        for (int i = 0; i < openBrackets; i++) {
            output = "    " + output;
        }
        return output;
    }

    public void format(IScanner scan, IWriter writer) throws IOException {
        int openBrackets = 0;
        char curChar = 0;
        char prevChar = 0;
        boolean isNewLine = false;
        if (scan.isNotFinished()) {
            curChar = scan.getNextChar();
            writer.append(curChar);
        }
        while (scan.isNotFinished()) {
            prevChar = curChar;
            curChar = scan.getNextChar();
            String output = String.valueOf(curChar);
            switch (curChar) {
                case '{':
                    output = output + "\n";
                    if (isNewLine) {
                        output = tab(openBrackets, output);
                    }
                    openBrackets++;
                    isNewLine = true;
                    break;
                case '}':
                    openBrackets--;
                    output = output + "\n";

            }
        }
    }
}
