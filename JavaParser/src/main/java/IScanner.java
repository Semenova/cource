import java.io.IOException;

/**
 * Created by Svetlana on 18.11.2015.
 */
public interface IScanner {
    public boolean isNotFinished();

    /**
     * Returns next char if exists
     * @return a next char
     * @throws IOException when sequence is finished or we have i/o error
     */
    public char getNextChar() throws IOException;
}
